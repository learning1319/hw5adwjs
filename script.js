
  class Card {
    constructor(value) {
      this.value = value;
      this.element = this.createCardElement();
      this.deleteButton = this.element.querySelector('.delete-button');
      this.listenerDelete();
    }

    createCardElement() {
      const card = document.createElement('div');
      card.classList.add('card');

      const title = document.createElement('h3');
      title.textContent = this.value.title;

      const text = document.createElement('p');
      text.textContent = this.value.body;

      const userInfo = document.createElement('div');
      userInfo.classList.add('user-info');
      userInfo.innerHTML = `
        <h2>${this.value.user.name} ${this.value.user.surname}</h2>
        <p>Email: ${this.value.user.email}</p>
      `;

      const deleteButton = document.createElement('button');
      deleteButton.classList.add('delete-button');
      deleteButton.textContent = 'Delete';

      card.append(userInfo, title, text, deleteButton);
      document.querySelector(".cards").appendChild(card);

      return card;
    }

    listenerDelete() {
      this.deleteButton.addEventListener('click', () => this.deletePost());
    }

    deletePost() {
      
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
        method: 'DELETE'
      })
        .then(response => {
          if (response.ok) {
           
            this.element.style.display = 'none';
          } else {
            console.error('Failed to delete post:', response.status);
          }
        })
        .catch(error => console.error('Error deleting post:', error));
    }
  }

  function fetchAndCreateCards() {
    fetch('https://ajax.test-danit.com/api/json/users')
      .then(response => response.json())
      .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
          .then(response => response.json())
          .then(posts => {
            
            const usersMap = new Map(users.map(user => [user.id, user]));
            const postsWithUserInfo = posts.map(post => ({
              ...post,
              user: usersMap.get(post.userId)
            }));

        
            postsWithUserInfo.forEach(post => new Card(post));
          })
          .catch(error => console.error('Error fetching posts:', error));
      })
      .catch(error => console.error('Error fetching users:', error));
  }


  fetchAndCreateCards();

